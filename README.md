# Android Hello

This project contains a basic Android application which displays a basic greeting.
The greeting is generated from a json which contains a first name, a last name, and
an age.

The .gitlab-ci.yml file adds [SAST](https://docs.gitlab.com/ee/user/application_security/sast/) and [Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/) to the 
CI/CD pipeline to detect vulnerabilities before they make it into production. You can see the present
vulnerabilities within the present [MR](https://gitlab.com/gitlab-da/tutorials/security-and-governance/mobile/android-hello/-/merge_requests/1).

**Note:** To see the vulnerablities, you must clone this repository to your group and
recreate the merge request. [GitLab Ultimate](https://about.gitlab.com/pricing/ultimate/) is required.

## References

- [GitLab Android SAST Scanning](https://docs.gitlab.com/ee/user/application_security/sast/#supported-languages-and-frameworks)

- [GitLab Android Dependency Scanning](https://gitlab.com/explore/catalog/components/android-dependency-scanning)

- [GitLab Dependency List (SBOM)](https://docs.gitlab.com/ee/user/application_security/dependency_list/)

- [Create your first Android app Tutorial](https://developer.android.com/codelabs/basic-android-kotlin-compose-first-app#0)

- Project photo by [Behzad Ghaffarian](https://unsplash.com/@behz?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash) on [Unsplash](https://unsplash.com/photos/hello-neon-signage-screenshot-5JcdQfzR_YA?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash)