package com.gitlab.hello

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.gitlab.hello.ui.theme.HelloTheme
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue

// Store person in struct from json
class Person(
    val firstName: String,
    val lastName: String,
    var age: Int)

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContent {
            HelloTheme {
                Scaffold(modifier = Modifier.fillMaxSize()) { innerPadding ->
                    Greeting(
                            modifier = Modifier.padding(innerPadding)
                    )
                }
            }
        }
    }
}

@Composable
fun Greeting(modifier: Modifier = Modifier) {
    val mapper = jacksonObjectMapper()
    val person: Person = mapper.readValue("{\"firstName\":\"Bob\", \"lastName\":\"Tanuki\", \"age\":50}")

    Surface (color = Color.Cyan) {
        Text(
            text = "Hello, I'm ${person.firstName} ${person.lastName}, age: ${person.age}",
            modifier = modifier.padding(24.dp)
        )
    }
}